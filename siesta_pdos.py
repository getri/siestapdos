#!/bin/env python
# Projected density of states (PDOS) calculated with SIESTA
# SIESTA http://www.icmab.es/siesta
# G. Tritsaris, 2012 :: georgios@tritsaris.eu

import pylab as py
import numpy as np
from sys import argv
from lxml import etree

#get label
if len(argv) == 1:
    print 'Syntax: %s Label [atom range]' % argv[0]
    exit()
else:
    label = argv[1]

#get atoms range
if len(argv) > 2:
    atoms = argv[2]
else:
    atoms = 'all'

#read PDOS file, number of atoms, and energy range
try:
    pdosdata = etree.parse('%s.PDOS' % label)
except:
    print 'Error reading %s.PDOS' % label
    exit()
Natoms=int(pdosdata.findall('orbital')[-1].items()[1][1])

energies = pdosdata.findtext('energy_values').split()
energies = np.array([float(x) for x in energies])

if atoms == 'all':
    atoms = range(1,Natoms+1)
elif '-' in atoms:
    try:
        atoms = atoms.split('-')
        atoms = range(int(atoms[0]), int(atoms[1])+1)
    except:
        atoms=range(1,Natoms+1)
else:
    atoms = [int(x) for x in atoms.split(',')]
    atoms.sort()

#read fermi level
try:
    f = open('%s.EIG' % label)
except:
    print 'Error reading %s.EIG' % label
    exit()
fermi = float(f.readline().split()[0])
f.close()

#shift energies with respect to fermi level
energies -= fermi

#assign orbitals to atoms
indices = {}
for i, orbital in enumerate(pdosdata.iterfind('orbital')):
    atom_index = int(orbital.items()[1][1])
    if not atom_index in atoms:
        continue

    index = int(orbital.items()[0][1])
    if indices.has_key(atom_index):
        indices[atom_index] += [index]
    else:
        indices[atom_index] = [index,]

#calculate PDOS on atoms
pdos = {}
maxpdos = 0
for atom in indices.iterkeys():
    for i, orbital in enumerate(pdosdata.iterfind('orbital/data')):
        if not i in indices[atom]:
            continue
        pdosorbital = np.array([float(x) for x in orbital.text.split()])
        if pdos.has_key(atom):
            pdos[atom] += pdosorbital
        else:
            pdos[atom] = pdosorbital
        maxpdos = max(maxpdos, max(pdos[atom]))

#plot PDOS
maxpdos *= 1.10
for atom in indices.iterkeys():
    py.plot(energies, pdos[atom])

py.plot((0, 0),(0,maxpdos),'k-')

py.xlim([min(energies),max(energies)])
py.ylim([0,maxpdos])

py.title('%s: Projected density of states' % label)
py.xlabel('Energy (eV)')
py.ylabel('Project DOS (arb.un.)')
py.grid(True)

py.savefig('%s_PDOS.png' % label, dpi = 200)

